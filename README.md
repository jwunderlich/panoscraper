# PanoScraper - minimalist webcam image loader

PanoScraper - simple live web image loader to use live web (e.g. panoramic) images in other programs.  
Written in Free Pascal / Lazarus

 Functions:
 - save live web images on local storage periodically with extra random parameter  
 - fetch images only by day  
 - archives images if required (with a timestamp)     
 - archives images only above a specified size (preventing foggy images)  
 - shows an event list to monitor the program function 
 - store configuration locally in File .panoscraper
 - Distinguish between civil and nautical twilight

PanoScraper has been extended with a tab menu structure to make the functions easier to handle: 
 - Status tab: Shows last system events and major parameters
 - Event log tab: Shows important system events
 - Configuration tab: Enables the configuration of various program parameters

  

![](images/panoscraper_004.png)  
**PanoScraper with its minimalist UI - Status Tab  



![](images/panoscraper_005.png)  

**PanoScraper - Event Log Tab  

 

![](images/panoscraper_006.png)  

**PanoScraper - Configuration Tab  



**How it Works**  

PanoScraper is a very minimalistic tool to periodically download images from the web, useful for webcam images. The parameters of the program are stored in a local configuration file (.panoscraper).  

The Program loads only Images from the web by daylight, more precisely between nautical dawn and nautical nightfall times. The downloaded image is stored in the working directory of the program.   
 If the "Save Images" checkbox is checked, the downloaded (webcam) image will archived with its timestamp in the filename. Only images above a specified size are saved. Thus, foggy or dark (night) images are ignored. Note, that the program does not handle the saved images in any way, so you have to ensure that the storage space does not overflow.  
 The edit-field "Image Url" contains the web adress of the webcam image. This parameter is stored in the source code of the program. 
 The download period with extra random parameter is specified in the code. The extra random parameter can be very useful to prevent detecting you as automatism. Some webcam sites lock ip-adresses with suspition of automation.  
 The "Events" window lists the important events of the program. With the button "Header Info" you can get some information over the specified image on the web server and the button "Download Now" downloads the required image immediately.  

![](images/panoimage-2021-09-03_08-24.m.jpg)  
![](images/panoimage-2021-08-19_12-53.m.jpg)  
![](images/panoimage-2021-06-14_21-23.m.jpg)  
![](images/panoimage-2021-07-06_13-26.m.jpg)  
**some result of PanoScraper

PanoScraper is used as part of the ![InfoDisplay](https://gitlab.com/jwunderlich/infodisplay/-/tree/master) project to actualize the webcam image in the weather display.   
Here some screenshots:  


![](images/screenshot-2021-08-19_12-51-49.m.png) --- ![](images/screenshot-2021-06-14_21-33-50.m.png)

  

__________
**Notes** 

** Third party libraries:  
TSunTime v1.11 -- by Kambiz R. Khojasteh  
Calculates times of sunrise, sunset, and solar noon.  
kambiz_at_delphiarea.com / www_.delphiarea.com  

** Compiler Settings:  
To compile and run the program on Raspberry Pi with Raspbian OS, you have to change the compiler and build target configuration in Lazarus IDE.

![](images/Options_for_Project__panoscraper_001.png)  
**Compiler target settings for Rasperry Pi2 with 32 Bit Raspbian in the Lazarus IDE (Project -> Options)  


​     

