unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, ComCtrls,
  StdCtrls, Spin, XMLPropStorage, ButtonPanel, Grids, fphttpclient,
  opensslsockets, FileUtil, ExtendedTabControls, openssl, fpopenssl,
  sunRiseSetUnit, DateUtils, StrUtils, typinfo, Process, Crt;


//{$R data.rc}
//not working with ARM Target (Raspberry)
//use instead: Project->Options->Resources
//https://wiki.freepascal.org/IDE_Window:_Project_Options#Resources

type

  //Header Data of http(s) page
  WebHeaderType = record
    ConnTime: TDateTime; //Date and Time of Connection
    ConnResult: string; //Result of Connection
    ImageDateStr: string; //Date of Panoramic Image as String
    ImageDate: TDateTime; //Date of Panoramic Image
    ImageSizeStr: string; //Size of Panoramic Image as String
    ImageSize: integer; //Size of Panoramic Image
  end;

  { TfrmMain }

  TfrmMain = class(TForm)
    AppProps: TApplicationProperties;
    btnDownloadNow: TButton;
    btnHeaderInfo: TButton;
    btnScrollToEnd: TButton;
    chkMinSizeByDay: TCheckBox;
    chkArchiveImages: TCheckBox;
    chkDlOnlyByDay: TCheckBox;
    chkMinSizeByDawn: TCheckBox;
    edtImgUrl: TLabeledEdit;
    edtMinImgSizeDay: TSpinEdit;
    edtMinImgSizeDawn: TSpinEdit;
    grpImage: TGroupBox;
    lblEvents: TLabel;
    lblkB: TLabel;
    lblkB1: TLabel;
    memEvents: TMemo;
    PageControl1: TPageControl;
    grdStatus: TStringGrid;
    tsStatus: TTabSheet;
    tsEventLog: TTabSheet;
    tsConfig: TTabSheet;
    tDLinterval: TIdleTimer;
    trayIcon: TTrayIcon;
    XMLPropStorage1: TXMLPropStorage;
    procedure btnDownloadNowClick(Sender: TObject);
    procedure btnHeaderInfoClick(Sender: TObject);
    procedure btnScrollToEndClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure memEventsChange(Sender: TObject);
    procedure tDLintervalTimer(Sender: TObject);
    procedure ftrayIconClick(Sender: TObject);

    //Check if Download Premitted or not
    function PermitCyclicDownload: boolean;

    //fetch PanoImage Data from https Header
    function getWebHeader(WebAddress: string): WebHeaderType;

    //download panorama image from the web (calling getWebHeader before)
    function DownloadPanoImage(WebAddress: string): integer;

    //set Font in App at Runtime
    procedure SetFontProperties(Control: TControl; FntName: TFontName;
      Size: integer; Styles: TFontStyles);

  private


  public

  end;

const
  DATA_ = 1;  //Data COLUMN in Status StringGrid

  STORAGE_ = 0; //Storage ROW in Status StringGrid
  NDAWN_ = 1;    //Nautical Dawn Time ROW in Status StringGrid
  CDAWN_ = 2;    //Civil Dawn Time ROW in Status StringGrid
  CNIGHTFALL_ = 3;    //Civil Nightfall Time ROW in Status StringGrid
  NNIGHTFALL_ = 4;    //Nautical Nightfall Time ROW in Status StringGrid
  IMGSIZE_ = 5;    //Web Image Size ROW in Status StringGrid
  IMGDATE_ = 6;    //Web Image Date ROW in Status StringGrid
  LASTIMGDATE_ = 7; //Last Downloaded Image Date ROW in Status StringGrid
  LASTMSG_ = 8;    //Last msg ROW in Status StringGrid
  NEXT_ = 9;    //Next Activity ROW in Status StringGrid

  {
   //Function Result of
   rDAY_ = 0;
   rNIGHTFALL_ = 1;
   rDAWN_ = 2;
   rNIGHT_ = 3;
  }



var
  frmMain: TfrmMain;
  lastpfdate: string; //last panorama file date
  lastpfsize: string; //last panorama file size
  CamInterval: integer; //Webcam Scraping Interval in Seconds

  WebImgInfo: WebHeaderType; //last Panoramic Image Web Header
  //previous Panoramic Image Hader before, will be refresh before requesting new Header:
  WebImgInfo_prev: WebHeaderType;
  WebImgInfo_dl: WebHeaderType; //last Downloaded Panoramic Image Data

  LastError: string; //Last Error String




implementation

{$R *.lfm}

var
  startflag: boolean;
  ndawn, nnightfall: string; //nautical dawn/nightfall
  cdawn, cnightfall: string; //civil dawn/nightfall
  fname, ext: string;  //standard filename of panoramic image (panoimage, jpg)
  fname_prev: string; //panoramic image filename before
  fullfname: string;
  lfullfname_ts: string; //full file Name with added timestamp at the end.
  // fname_ts: string;

  _sun_rise_nautical, _sun_set_nautical: TDateTime;  //Nautical Sunrise/Sunset
  _sun_rise_civil, _sun_set_civil: TDateTime; //Civil Sunrise/Sunset
  lat, lon: double;
  date: TDateTime;
  timenow: string;

  targetDir: ansistring;
  ImgUrl: string;
  ts: string; //Timestamp of Now


procedure delay_(const ms: double);
begin
  repeat
    application.ProcessMessages;
  until now > ms;
end;

//Set Font at Runtime
//https://stackoverflow.com/questions/10588660/font-consistency-throughout-project
//Using: SetFontProperties(Self, 'Ubuntu Condensed', 10, []);
//Checking for Condensed Fonts on Linux via Terminal Command:
//fc-list |grep 'Condensed'
procedure TFrmMain.SetFontProperties(Control: TControl; FntName: TFontName;
  Size: integer; Styles: TFontStyles);
// Set font properties
var
  Index: integer;
  FntFont: TFont;
  AnObject: TObject;
  ChildControl: TControl;
begin
  // Set FntFont properties
  AnObject := GetObjectProp(Control, 'Font', nil);
  if AnObject is TFont then
  begin
    // Set properties
    FntFont := TFont(AnObject);
    FntFont.Name := FntName;
    FntFont.Size := Size;
    FntFont.Style := Styles;
  end;

  // Set child FntFont properties
  if Control is TWinControl then
  begin
    // Set
    for Index := 0 to TWinControl(Control).ControlCount - 1 do
    begin
      // Child control
      ChildControl := TWinControl(Control).Controls[Index];

      // Set FntFont properties
      SetFontProperties(ChildControl, FntName, Size, Styles);
    end;
  end;
end;


procedure GetNauticalSun;
const
  //twilight phases
  zenithOfficial = 0;
  zenithCivil = 1;
  zenithNautical = 2;
  zenithAstronomical = 3;
var
  offset: integer;
  Err: string;
begin
  Err := 'Error calculating Twilight';
  LastError := Err;
  //grdStatus.Cells[DATA_, LASTMSG_] := LastError;
  offset := trunc((GetLocalTimeOffset * -1) / 60);
  _sun_rise_nautical := sunRiseSet(0, date, lat, lon, Err, zenithNautical, offset);
  if (_sun_rise_nautical = 0) then
    ShowMessage(Err);
  _sun_set_nautical := sunRiseSet(1, date, lat, lon, Err, zenithNautical, offset);
  if (_sun_set_nautical = 0) then
    ShowMessage(Err);
end;


procedure GetCivilSun;
const
  //twilight phases
  zenithOfficial = 0;
  zenithCivil = 1;
  zenithNautical = 2;
  zenithAstronomical = 3;
var
  offset: integer;
  Err: string;
begin
  Err := 'Error calculating Twilight';
  LastError := Err;
  //grdStatus.Cells[DATA_, LASTMSG_] := LastError;
  offset := trunc((GetLocalTimeOffset * -1) / 60);
  _sun_rise_civil := sunRiseSet(0, date, lat, lon, Err, zenithCivil, offset);
  if (_sun_rise_civil = 0) then
    ShowMessage(Err);
  _sun_set_civil := sunRiseSet(1, date, lat, lon, Err, zenithCivil, offset);
  if (_sun_set_civil = 0) then
    ShowMessage(Err);
end;

function isDay_naut: boolean;

begin
  Result := False;

  //Set Coordinates to calculate twilight
  lat := 48.035;
  lon := 11.213;
  date := Now;

  try
    GetNauticalSun;

    ndawn := TimeToStr(_sun_rise_nautical);
    nnightfall := TimeToStr(_sun_set_nautical);
    timenow := TimeToStr(Now);
    //Check if time between dawn and nightfall
    if (timenow >= ndawn) and (timenow <= nnightfall) then
      Result := True;

  except
    // Result := True;
    //ShowMessage('Failure in isDay Function');
    LastError := 'Failure in isDay_naut Function';
  end;

end;

function isDay_civil: boolean;

begin
  Result := False;

  //Set Coordinates to calculate twilight
  lat := 48.035;
  lon := 11.213;
  date := Now;

  try
    GetCivilSun;

    cdawn := TimeToStr(_sun_rise_civil);
    cnightfall := TimeToStr(_sun_set_civil);
    timenow := TimeToStr(Now);
    //Check if time between dawn and nightfall
    if (timenow >= cdawn) and (timenow <= cnightfall) then
      Result := True;

  except
    // Result := True;
    //ShowMessage('Failure in isDay Function');
    LastError := 'Failure in isDay_civil Function';
  end;

end;

// Create Semaphore Files on Rpi /mnt/ramdisk/
// to give the ability to control other programs
// with panoscraper
function SetSemaphores: boolean;
var
  dayfh: THandle; //day filehandle
  nightfh: THandle; //night filehandle
  DirPath, fullDirPath: string;
  dayf, nightf: string;

begin
  Result := False;
  DirPath := '/mnt/ramdisk';
  fullDirPath := DirPath + '/';
  dayf := 'day';
  nightf := 'night';

  if DirectoryExists(DirPath) then
  begin
    if isDay_naut then
    begin
      if FileExists(fullDirPath + nightf) then
        DeleteFile(fullDirPath + nightf);
      if not FileExists(fullDirPath + dayf) then
      begin
        dayfh := FileCreate(fullDirPath + dayf);
        FileClose(dayfh);
      end;
    end
    else
    begin
      if FileExists(fullDirPath + dayf) then
        DeleteFile(fullDirPath + dayf);
      if not FileExists(fullDirPath + nightf) then
      begin
        nightfh := FileCreate(fullDirPath + nightf);
        FileClose(nightfh);
      end;
    end;
  end
  else
  begin
  end;
end;



//Check if Download Premitted or not
function TfrmMain.PermitCyclicDownload: boolean;
var
  ts: string; //Timestamp of Now

begin
  Result := True;
  ts := FormatDateTime('yyyy-mm-dd_hh-nn-ss', Now); //create Timestamp String
  if not isDay_naut and chkDlOnlyByDay.Checked then
  begin
    memEvents.Lines.Add(ts +
      ' Download Not Permitted, due to [Download only by Day] in cfg');
    grdStatus.Cells[DATA_, LASTMSG_] :=
      'Download Not Permitted ([Download only by Day] in cfg)';
    Result := False;
    Exit;
  end;
  WebImgInfo := getWebHeader(ImgUrl);
  if WebImgInfo.ImageSize < edtMinImgSizeDay.Value * 1000 then
  begin
    memEvents.Lines.Add(ts + ' Download Not Permitted, due to [min Image Size] in cfg');
    grdStatus.Cells[DATA_, LASTMSG_] :=
      ts + ' Download Not Permitted ([min Image Size (' + edtMinImgSizeDay.Text +
      'kB)]) in cfg. (Img=' + IntToStr(Trunc(WebImgInfo.ImageSize / 1000)) + 'kB)';
    memEvents.Lines.Add(ts + '  Web Image: ' + IntToStr(
      Trunc(WebImgInfo.ImageSize / 1000)) + ' kB, min Required: ' +
      edtMinImgSizeDay.Text + ' kB');
    Result := False;
  end;

end;     //PermitDownload



//fetch PanoImage Data from https Header
//-Image File Modification Date in Web
//-Image File Size in Web
function TfrmMain.getWebHeader(WebAddress: string): WebHeaderType;
var
  ARec: WebHeaderType;
  pfdatestr: string; //new panorama file date as string
  pfsizestr: string; //new panorama file size as string
  client: TFPHTTPClient; //Website to poll
  s: string;
  si: integer;
  uagent: string; //Let the server think you are a browser ;)
  { https header format:
  Server: nginx
  Date: Tue, 06 Apr 2021 16:50:06 GMT
  Content-Type: image/jpeg
  Content-Length: 205965
  Last-Modified: Tue, 06 Apr 2021 16:48:42 GMT
  Connection: close
  ETag: "606c90ea-3248d"
  Expires: Tue, 06 Apr 2021 16:55:06 GMT
  Cache-Control: max-age=300
  Cache-Control: no-cache
  Accept-Ranges: bytes
  }
  ts: string; //TimeStamp of Now

begin
  //https://wiki.lazarus.freepascal.org/fphttpclient
  ARec.ConnTime := 0;
  ARec.ConnResult := 'ok';
  ARec.ImageDate := 0;
  ARec.ImageDateStr := '';
  ARec.ImageSize := 0;
  ARec.ImageSizeStr := '';
  ts := FormatDateTime('yyyy-mm-dd_hh-nn-ss', Now); //create Timestamp String

  // get page info from Https header

  try
    try
      client := TFPHTTPClient.Create(nil);
      client.AllowRedirect := True;
      //prepare request
      uagent := 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0';
      client.AddHeader('User-Agent', uagent);
      //prepare http header
      client.HTTPMethod('HEAD', ImgUrl, nil, [200]);

      //Store this Request Time
      ARec.ConnTime := Now;

      //fire http header request and analyze fetched http header
      for s in client.ResponseHeaders do
      begin
        if AnsiContainsStr(s, 'Content-Length') then
        begin
          si := pos(#32, s);
          if si > 0 then
          begin
            pfsizestr := AnsiMidStr(s, si + 1, 50);
            //get Panorama Image File Size from https header as String
            ARec.ImageSizeStr := pfsizestr;
            //build Panorama Image File Size String from https header as Integer;
            ARec.ImageSize := StrToInt(pfsizestr);
          end;
        end;

        if AnsiContainsStr(s, 'Last-Modified') then
        begin
          si := pos(#32, s);
          if si > 0 then
          begin
            pfdatestr := AnsiMidStr(s, si + 1, 50);
            //get Panorama Image File modification date from https header as String
            ARec.ImageDateStr := pfdatestr;
            //build Panorama Image File modification date from https header String as TDateTime
            //ARec.ImageDate:= StrToDate(pfdatestr);
          end;
        end;
      end;
      //https://wiki.freepascal.org/Exceptions
    finally
      client.Free;
    end;
  except
    //on E: EHttpClient do
    //begin
    //ARec.ConnResult := e.message;
    ARec.ConnResult := 'Error';
    Application.ProcessMessages;
    memEvents.Lines.Add(ts + ' ' + ARec.ConnResult + ' in Function getWebHeader');
    //ShowMessage('Error in Function getWebHeader: ' + ARec.ConnResult);
    LastError := ts + ' ' + ARec.ConnResult + ' in Function getWebHeader';
    grdStatus.Cells[DATA_, LASTMSG_] := LastError;
    //end;
    //end
    //else
    //  ARec.ConnResult := 'connection_error';
  end;



  //Store WebImgInfo Record to WebImgInfo_prevore to able comparing previous state
  WebImgInfo_prev := WebImgInfo;
  //Generate Result Type Record
  Result := ARec;
  //push result in Global Type too
  WebImgInfo := ARec;
  grdStatus.Cells[DATA_, IMGDATE_] := ARec.ImageDateStr;
  grdStatus.Cells[DATA_, IMGSIZE_] := ARec.ImageSizeStr + ' Bytes';
end; //function getWebHeader(WebAddress: string): WebHeaderType;



//download panorama image from the web
//Results:
//0:     New Image Downloaded
//1:  No new Image available
//2:  Error Downloading new Image
function TFrmMain.DownloadPanoImage(WebAddress: string): integer;
var
  ts: string; //TimeStamp of Now
  lfname_ts: string; // temporary result

begin
  //https://wiki.lazarus.freepascal.org/fphttpclient
  Result := 2;
  ts := FormatDateTime('yyyy-mm-dd_hh-nn-ss', Now); //create Timestamp String
  lfname_ts := fname + '-' + ts;
  fullfname := fname + ext;
  lfullfname_ts := lfname_ts + ext;

  try
    //get Header
    WebImgInfo := getWebHeader(ImgUrl);
    if WebImgInfo.ConnResult = 'ok' then
    begin //Download only if file with newer Date exists
      if WebImgInfo.ImageDateStr <> WebImgInfo_dl.ImageDateStr then
      begin
        //copy existing file to prev-file
        CopyFile(targetDir + fullfname, targetDir + fname_prev);
        //Get and save Web Image
        TFPHTTPClient.SimpleGet(ImgUrl, targetDir + fullfname);
        Application.ProcessMessages;

        //save last WebImgInfo Header Data to Downloaded WebImgInfo Data 'WebImgInfo_dl'
        WebImgInfo_dl := WebImgInfo;
        grdStatus.Cells[DATA_, LASTIMGDATE_] := WebImgInfo_dl.ImageDateStr;

        if chkArchiveImages.Checked then
        begin
          //copy downloaded file to timestamped file
          CopyFile(targetDir + fullfname, targetDir + lfullfname_ts);
          memEvents.Lines.Add(ts + ' File Downloaded and Archived to ' + lfullfname_ts);
        end

        else
          memEvents.Lines.Add(ts +
            ' File Downloaded but not Archived due to [Save Images] in cfg');

        Result := 0;
      end
      else //no New Image on Web
        Result := 1;
    end
    else
      //Result is False, if Header Result is Bad
      Result := 2;

  except
    //Result is False on Exception
    Result := 2;
    memEvents.Lines.Add(ts + ' Exception in Function DownloadPanoImage');
    LastError := ts + ' Exception in Function DownloadPanoImage';
    grdStatus.Cells[DATA_, LASTMSG_] := LastError;
  end;
end;  //function DownloadPanoImage


procedure TfrmMain.FormCreate(Sender: TObject);
var
  ts: string; //TimeStamp of Now
  lfname_ts: string; // temporary result
  S: TResourceStream; //Resource Content Handling
  F: TFileStream;  //Resource Content Handling
  Process: TProcess; //for font cache refresh command (cmd)
  aProcess: TProcess; //restarting self
  fontexists: boolean;

begin
  fontexists := FileExists(GetUserDir + '/.fonts/ucondensed.ttf');


  if not fontexists then
  begin

    //     LOAD CUSTOM FONT
    //Create local Font Dir for Ubuntu Condensed Font, if not exists
    if not DirectoryExists(GetUserDir + '/.fonts') then
      if not CreateDir(GetUserDir + '/.fonts') then
        memEvents.Lines.Add(ts + ' Failed to create local font directory!')
      else
        memEvents.Lines.Add(ts + ' Local font directory created');

    //Using resources:
    //https://wiki.freepascal.org/Lazarus_Resources#Linux
    //https://wiki.freepascal.org/IDE_Window:_Project_Options#Resources
    //https://wiki.freepascal.org/fcl-res
    //extract the stored resource (font) into the file ucondensed.ttf_:
    // create a resource stream which points to our resource
    S := TResourceStream.Create(HInstance, 'UCONDENSED', RT_RCDATA);
    // Please ensure you write the enclosing apostrophes around DATA,
    // otherwise no data will be extracted.
    try
      // create a file ucondensed.ttf_ in the application directory
      //F := TFileStream.Create(ExtractFilePath(ParamStr(0)) + 'ucondensed.ttf_', fmCreate);
      F := TFileStream.Create(GetUserDir + '/.fonts/ucondensed.ttf', fmCreate);
      try
        F.CopyFrom(S, S.Size); // copy data from the resource stream to file stream
      finally
        F.Free; // destroy the file stream
      end;
    finally
      S.Free; // destroy the resource stream
    end;

    //Refresh Font Cache (for Ubuntu Condensed)
    Process := TProcess.Create(nil);
    //$ fc-cache -fv
    try
      Process.CommandLine := 'fc-cache -fr';
      Process.Options := Process.Options + [poWaitOnExit];
      Process.Execute;
      Process.Free;
    except
      Process.Free;
      //ShowMessage('File Date checking Process: Exception occured');
      memEvents.Lines.Add(ts + ' Error while refreshing Font Cache at Start');
    end; //try

    //Setting Font (Ubuntu Condensed from Resource File) Recursively for all components
    // Not Working on Linux, only in Windows: Application.DefaultFont := 'Ubuntu Condensed';
    try
      SetFontProperties(Self, 'Ubuntu Condensed', 12, []);
      //SetFontProperties(Self, 'Nimbus Sans Narrow Regular', 10, []);
    except
      memEvents.Lines.Add(ts + ' Font [Ubuntu Condensed] does not exists');
    end;

    //after copying new font, restart Application to apply changes
    Delay(2000);
    aProcess := TProcess.Create(nil);
    aProcess.CommandLine := '"' + Application.ExeName + '"';
    aProcess.Execute;
    aProcess.Free;
    Application.Terminate;
  end
  else
    SetFontProperties(Self, 'Ubuntu Condensed', 12, []);

  //     END LOAD CUSTOM FONT


  XMLPropStorage1.FileName := ExtractFilePath(ParamStr(0)) + '.' +
    ExtractFileName(ParamStr(0));
  XMLPropStorage1.Restore;
  frmMain.Width := 700;
  //Set Cell Font Color to red?
  //grdStatus.Cells[DATA_,LASTMSG_].;
  ext := '.jpg';
  fname := 'panoimage';
  fname_prev := 'panoimagebefore';
  memEvents.Clear;

  ImgUrl := edtImgUrl.Text;
  isDay_naut;
  isDay_civil;

  ts := FormatDateTime('yyyy-mm-dd_hh-nn-ss', Now); //create Timestamp String
  lfname_ts := fname + '-' + ts;
  fullfname := fname + ext;
  lfullfname_ts := lfname_ts + ext;

  // Nautical Dawn time
  grdStatus.Cells[DATA_, NDAWN_] := ndawn;
  // Civil Dawn time
  grdStatus.Cells[DATA_, CDAWN_] := cdawn;
  // Civil Nightfall time
  grdStatus.Cells[DATA_, CNIGHTFALL_] := cnightfall;
  // Nautical Nightfall time
  grdStatus.Cells[DATA_, NNIGHTFALL_] := nnightfall;

  memEvents.Lines.Add(ts + ' Program start: ' + DateTimeToStr(Now));
  memEvents.Lines.Add(ts + ' Nautical Dawn Time: ' + ndawn);
  memEvents.Lines.Add(ts + ' Nautical Nightfall Time: ' + nnightfall);
  startflag := True;
  //frmMain.WindowState := wsMinimized;
  targetDir := GetCurrentDir + '/';
  //Storage
  grdStatus.Cells[DATA_, STORAGE_] := targetDir;
  //Starting Cyclic Download
  tDLintervalTimer(Self);

  SetSemaphores;

end;



procedure TfrmMain.btnHeaderInfoClick(Sender: TObject);
var
  ts: string; //TimeStamp of Now

begin
  ts := FormatDateTime('yyyy-mm-dd_hh-nn-ss', Now); //TimeStamp

  WebImgInfo := getWebHeader(ImgUrl);

  if WebImgInfo.ConnResult <> 'ok' then
  begin
    memEvents.Lines.Add(ts + ' Error: ' + WebImgInfo.ConnResult);
    LastError := ts + ' Error: ' + WebImgInfo.ConnResult;
    grdStatus.Cells[DATA_, LASTMSG_] := LastError;
  end
  else
  begin
    memEvents.Lines.Add(ts + ' last FileSize: ' + WebImgInfo.ImageSizeStr);
    memEvents.Lines.Add(ts + ' last FileDate: ' + WebImgInfo.ImageDateStr);
  end;

end;

procedure TfrmMain.btnScrollToEndClick(Sender: TObject);
begin
  //Scroll memo to the End
  memEvents.CaretPos := Point(0, memEvents.Lines.Count - 1);
end;


// Download Panoramic Image manually from Server
procedure TfrmMain.btnDownloadNowClick(Sender: TObject);
var
  ts: string; //TimeStamp of Now

begin
  ts := FormatDateTime('yyyy-mm-dd_hh-nn-ss', Now);

  //WebImgInfo := getWebHeader(ImgUrl);
  //header will requested during Download
  case DownloadPanoImage(ImgUrl) of
    0:
    begin
      memEvents.Lines.Add(ts + ' Forced Download Success: ' + lfullfname_ts);
      //copy downloaded file to timestamped file
      CopyFile(targetDir + fullfname, targetDir + lfullfname_ts);
    end;
    1: memEvents.Lines.Add(ts + ' No New Image in Web on Forced Download');
    2:
    begin
      memEvents.Lines.Add(ts + ' Download ERROR on Forced Download');
      LastError := ts + ' Download ERROR on Forced Download';
      grdStatus.Cells[DATA_, LASTMSG_] := LastError;
    end;
  end;

end; //btnDownloadNowClick

procedure TfrmMain.FormActivate(Sender: TObject);
begin
  if startflag = True then
  begin
    //Application.Minimize;
    startflag := False;
  end;
  //Scroll memo to the End
  memEvents.CaretPos := Point(0, memEvents.Lines.Count - 1);
end;

procedure TfrmMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  XMLPropStorage1.Save;
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  XMLPropStorage1.Save;
end;



procedure TfrmMain.FormShow(Sender: TObject);
begin

  //Scroll memo to the End
  memEvents.CaretPos := Point(0, memEvents.Lines.Count - 1);
end;

procedure TfrmMain.memEventsChange(Sender: TObject);
begin
  //Scroll memo to the End
  memEvents.CaretPos := Point(0, memEvents.Lines.Count - 1);
end;

//CYCLIC TIMER ACTIONS
procedure TfrmMain.tDLintervalTimer(Sender: TObject);
const
  RandomInterval = 300; //Random Time Interval in Seconds to Obscure Automatism
var
  ts: string; //Timestamp of Now
  lfname_ts: string; // temporary result

begin
  ts := FormatDateTime('yyyy-mm-dd_hh-nn-ss', Now); //create Timestamp String
  lfname_ts := fname + '-' + ts;
  fullfname := fname + ext;
  lfullfname_ts := lfname_ts + ext;

  CamInterval := 600; //Camera Scraping Interval in Seconds

  // actual: every 15 Minutes + Random Value btw. 0-5 Minutes
  // Interval := 600000 + Random(300000)
  tDLinterval.Enabled := False;
  Randomize;
  // change the Interval to obscure Automatism
  tDLinterval.Interval := CamInterval * 1000 + Random(RandomInterval * 1000);

  if PermitCyclicDownload then
  begin
    case DownloadPanoImage(ImgUrl) of
      0:
      begin
        memEvents.Lines.Add(ts + ' Cyclic Download Success: ' + lfullfname_ts);
        grdStatus.Cells[DATA_, LASTMSG_] :=
          ts + ' Cyclic Download Success: ' + lfullfname_ts;
      end;
      1:
      begin
        memEvents.Lines.Add(ts + ' No New Image in Web on Cyclic Download');
        grdStatus.Cells[DATA_, LASTMSG_] :=
          ts + ' No New Image in Web on Cyclic Download';
      end;
      2:
      begin
        memEvents.Lines.Add(ts + ' Download ERROR on Cyclic Download');
        LastError := ts + ' Download ERROR on Cyclic Download';
        grdStatus.Cells[DATA_, LASTMSG_] := LastError;
      end;
    end;
  end;
  //else //handled in Function PermitCyclicDownload
  //  memEvents.Lines.Add(ts + ' Cyclic Download not Permitted due to Configuration');

  memEvents.Lines.Add(ts + ' Next Cyclic Download in ' + IntToStr(
    Trunc(tDLinterval.Interval / 1000 / 60)) + ' Min');

  grdStatus.Cells[DATA_, NEXT_] :=
    ts + ': Next Cyclic Check in ' +
    IntToStr(Trunc(tDLinterval.Interval / 1000 / 60)) + ' Min';

  SetSemaphores;

  tDLinterval.Enabled := True;
end;

procedure TfrmMain.ftrayIconClick(Sender: TObject);
begin
  frmMain.Show;
end;


end.
